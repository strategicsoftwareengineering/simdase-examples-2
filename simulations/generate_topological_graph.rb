require 'csv'

print "Enter path to csv output file: "
file = gets.chomp
print "Which module? "
module_name = gets.chomp
print "Which time period? "
time_period = gets.chomp
print "Which column for X? "
x_col = gets.chomp
print "Which column for Y? "
y_col = gets.chomp
print "Multiplier? "
multiplier = gets.chomp.to_i
print "Which columns contain cost for X (comma separated for multiple)? "
x_costs_col = gets.chomp.split(",")
print "Which columns contain cost for Y (comma separated for multiple)? "
y_costs_col = gets.chomp.split(",")

data = {}

CSV.foreach(file, :headers => true) do |row|
	if row["module_name"] == module_name and row["t"] == time_period
		x = (row[x_col].to_f * multiplier).to_i
		y = (row[y_col].to_f * multiplier).to_i
		if data[x] == nil
			data[x] = {}
		end
		hours = 0.0
		x_costs_col.each do |x_cost_col|
			row[x_cost_col].scan(/(\d+.\d+)/).flatten.each_slice(2) { |slice| hours += slice[0].to_f * slice[1].to_f}
		end
		y_costs_col.each do |y_cost_col|
			row[y_cost_col].scan(/(\d+.\d+)/).flatten.each_slice(2) { |slice| hours += slice[0].to_f * slice[1].to_f}
		end
		data[x][y] = hours
	end
end

puts "data <- c("
(1..multiplier).each do |x|
	print("c(")
	(1..multiplier).each do |y|
		if data[x-1]
			if data[x-1][y-1]
				print data[x-1][y-1]
			else
				print "0.0"
			end
		else
			print "0.0"
		end
		if y != multiplier
			print ","
		end
	end
	print(")")
	if x == multiplier
		puts
	else
		puts ","
	end
end
puts ")"
puts 
puts "dim(data) <- c(#{multiplier},#{multiplier})"
puts "p <- plot_ly(showscale = FALSE) %>%"
puts "add_surface(z = ~data) %>%"
puts "layout(scene = list(xaxis = list(title = '#{x_col.gsub("_", " ").split.map(&:capitalize).join(' ')}'), yaxis = list(title = '#{y_col.gsub("_", " ").split.map(&:capitalize).join(' ')}'), zaxis = list(title = 'Hours Required')))"
puts "embed_notebook(p)"