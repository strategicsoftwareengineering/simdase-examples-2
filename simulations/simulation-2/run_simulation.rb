puts "iteration,t,module_name,requirements_engineering_scaling,architecture_definition_scaling,component_development_scaling,testing_scaling,requirements_engineering,requirements_engineering,architecture_definition,component_development,testing"

(1..ARGV[0].to_i).each do |iteration|
	t_start = 1
	t_end = 52

	(t_start..t_end).each do |period|
		#requirements_engineering => {
		#	2.0 "Business Analysts" for if(1.0 <= t && t <= 8.0) { ((Math.cos(t) + 1) * random()) * 40.0 } else { random() * 5.0 }	
		#};
		#architecture_definition => {
		#	2.0 "Software Architects" for if(8.0 <= t && t <= 20.0) { ((Math.cos(t) + 1) * random()) * 40.0 } else { random() * 20.0 }	
		#};
		#component_development => {
		#	3.0 "Software Developers" for if(20.0 <= t && t <= 45.0) { ((Math.cos(t) + 1) * random()) * 40.0 } else { random() * 20.0 }	
		#};
		#testing => {
		#	2.0 "Software Testers" for if(45.0 <= t && t <= 52.0) { ((Math.cos(t) + 1) * random()) * 40.0 } else { random() * 20.0 },
		#	2.0 "Hardware Testers" for if(45.0 <= t && t <= 52.0) { ((Math.cos(t) + 1) * random()) * 40.0 } else { random() * 20.0 }	
		#};

		requirements_engineering = ""
		architecture_definition = ""
		component_development = ""
		testing = ""

		requirements_engineering_scaling = Random.rand(10000) / 10000.0 # scaled randomly
		architecture_definition_scaling = Random.rand(10000) / 10000.0 # scaled randomly
		component_development_scaling = Random.rand(10000) / 10000.0 # scaled randomly
		testing_scaling = Random.rand(10000) / 10000.0 # scaled randomly

		if 1 <= period && period <= 8
			requirements_engineering = "\"2.0 Business Analysts for #{Math.cos(period) * requirements_engineering_scaling * 40.0}\""
		else
			requirements_engineering = "\"2.0 Business Analysts for #{requirements_engineering_scaling * 5.0}\""
		end
		if 8 <= period && period <= 20
			architecture_definition = "\"2.0 Software Architects for #{Math.cos(period) * architecture_definition_scaling * 40.0}\""
		else
			architecture_definition = "\"2.0 Software Architects for #{architecture_definition_scaling * 20.0}\""
		end
		if 20 <= period && period <= 45
			component_development = "\"3.0 Software Developers for #{Math.cos(period) * component_development_scaling * 40.0}\""
		else
			component_development = "\"3.0 Software Developers for #{component_development_scaling * 20.0}\""
		end
		if 45 <= period && period <= 52
			testing = "\"2.0 Software Testers for #{Math.cos(period) * testing_scaling * 40.0},2.0 Hardware Testers for #{Math.cos(period) * testing_scaling * 40.0}\""
		else
			testing = "\"2.0 Software Testers for #{testing_scaling * 20.0},2.0 Hardware Testers for #{testing_scaling * 20.0}\""
		end

		puts "#{iteration},#{period},meal_planner.impl,#{requirements_engineering_scaling},#{architecture_definition_scaling},#{component_development_scaling},#{testing_scaling},#{requirements_engineering},#{requirements_engineering},#{architecture_definition},#{component_development},#{testing}"
	end

end