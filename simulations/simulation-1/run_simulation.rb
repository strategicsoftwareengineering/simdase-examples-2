puts "iteration,t,module_name,software_architect_scaling,software_developer_scaling,organizational_planning,requirements_engineering,architecture_definition,architecture_evaluation,component_development,testing,software_system_integration"

(1..ARGV[0].to_i).each do |iteration|
	t_start = 0
	t_end = 9

	(t_start..t_end).each do |period|
		organizational_planning = ""
		requirements_engineering = ""
		architecture_definition = ""
		architecture_evaluation = ""
		component_development = ""
		testing = ""
		software_system_integration = ""

		business_analyst_scaling = 1.0
		software_architect_scaling = Random.rand(10000) / 10000.0 # scaled randomly
		software_developer_scaling = Random.rand(10000) / 10000.0 # scaled randomly
		tester_scaling = 1.0
		system_administrator_scaling = 1.0

		if period == 0 || period == 1
			organizational_planning = "\"2.0 Project Manager for 40.0\""
		end
		if period > 0 && period % 2 == 0
			requirements_engineering = "\"2.0 Business Analysts for #{business_analyst_scaling * 40.0}\""
			architecture_definition = "\"2.0 Software Architects for #{software_architect_scaling * 40.0}\""
			architecture_evaluation = "\"2.0 Software Architects for #{software_architect_scaling * 40.0}\""
		end
		if period > 1 && period % 2 == 1
			component_development = "\"3.0 Software Developers for #{software_architect_scaling * 20.0}\""
			testing = "\"2.0 Testers for #{tester_scaling * 40.0}\""
			software_system_integration = "\"1.0 System Administrator for #{system_administrator_scaling * 20.0}\""
		end
		if period > 1 && period % 2 == 0
			component_development = "\"3.0 Software Developers for #{software_developer_scaling * 40.0}\""
		end

		puts "#{iteration},#{period},fork_lift_tracker.impl,#{software_architect_scaling},#{software_developer_scaling},#{organizational_planning},#{requirements_engineering},#{architecture_definition},#{architecture_evaluation},#{component_development},#{testing},#{software_system_integration}"
	end

end