puts "iteration,t,module_name,requirements_engineering_scaling,architecture_definition_scaling,component_development_scaling,testing_scaling,full_scaling_factor,requirements_engineering,requirements_engineering,architecture_definition,component_development,testing"

(1..ARGV[0].to_i).each do |iteration|
	t_start = 1
	t_end = 52

	(t_start..t_end).each do |period|
		if period == 18
			#scaling_factor => 1.0/(i+1.0);
			#time_min => 1.0;
			#time_max => 52.0;
			#cost => {
			#	requirements_engineering => {
			#		2.0 "Business Analysts" for if(0.0 <= t && t <= 5.0) { ((Math.cos(t) + 1) * random()) * 40.0 } else { 5.0 * random() }	
			#	};
			#	architecture_definition => {
			#		3.0 "Software Architects" for if(5.0 <= t && t <= 20.0) { ((Math.cos(t) + 1) * random()) * 40.0 } else { 10.0 * random() }
			#	};
			#	component_development => {
			#		3.0 "Software Developers" for if(15.0 <= t && t <= 40.0) { ((Math.cos(t) + 1) * random()) * 40.0 } else { 10.0 * random() },
			#	};
			#	testing => {
			#		2.0 "Software Testers" for if(35.0 <= t && t <= 52.0) { ((Math.cos(t) + 1) * random()) * 40.0 } else { 5.0 * random() },
			#		2.0 "Hardware Testers" for if(15.0 <= t && t <= 25.0) { ((Math.cos(t) + 1) * random()) * 40.0 } else { 0.0 }
			#	};
			#};

			requirements_engineering = ""
			architecture_definition = ""
			component_development = ""
			testing = ""

			requirements_engineering_scaling = Random.rand(10000) / 10000.0 # scaled randomly
			architecture_definition_scaling = Random.rand(10000) / 10000.0 # scaled randomly
			component_development_scaling = Random.rand(10000) / 10000.0 # scaled randomly
			testing_scaling = Random.rand(10000) / 10000.0 # scaled randomly
			full_scaling_factor = 1.0/Math.log(Random.rand(100))

			if 1 <= period && period <= 5
				requirements_engineering = "\"2.0 Business Analysts for #{(Math.cos(period) * requirements_engineering_scaling * 40.0)**full_scaling_factor}\""
			else
				requirements_engineering = "\"2.0 Business Analysts for #{(requirements_engineering_scaling * 5.0)**full_scaling_factor}\""
			end
			if 5 <= period && period <= 20
				architecture_definition = "\"3.0 Software Architects for #{(Math.cos(period) * architecture_definition_scaling * 40.0)**full_scaling_factor}\""
			else
				architecture_definition = "\"3.0 Software Architects for #{(architecture_definition_scaling * 10.0)**full_scaling_factor}\""
			end
			if 15 <= period && period <= 40
				component_development = "\"3.0 Software Developers for #{(Math.cos(period) * component_development_scaling * 40.0)**full_scaling_factor}\""
			else
				component_development = "\"3.0 Software Developers for #{(component_development_scaling * 10.0)**full_scaling_factor}\""
			end
			if 15 <= period && period <= 25
				testing = "\"2.0 Hardware Testers for #{(Math.cos(period) * testing_scaling * 40.0)**full_scaling_factor}\""
			elsif 35 <= period && period <= 52
				testing = "\"2.0 Software Testers for #{(Math.cos(period) * testing_scaling * 40.0)**full_scaling_factor}\""
			else
				testing = "\"2.0 Software Testers for #{(testing_scaling * 5.0)**full_scaling_factor}\""
			end

			puts "#{iteration},#{period},energy_source.impl,#{requirements_engineering_scaling},#{architecture_definition_scaling},#{component_development_scaling},#{testing_scaling},#{full_scaling_factor},#{requirements_engineering},#{requirements_engineering},#{architecture_definition},#{component_development},#{testing}"
		end
	end

end