require 'csv'

print "Enter path to csv output file: "
file = gets.chomp

components = {}

CSV.foreach(file, :headers => true, :header_converters => :symbol) do |row|
	if components[row[:component_name]] == nil
		components[row[:component_name]] = {}
		components[row[:component_name]][row[:t].to_f] = {}
	else 
		components[row[:component_name]][row[:t].to_f] = {}
	end

	if row[:architecture_definition_cost] != ""
		hours = 0.0
		row[:architecture_definition_cost].scan(/(\d+.\d+) [^\d]+ at (\d+.\d+)/).flatten.each_slice(2) { |slice| hours += slice[0].to_f * slice[1].to_f}
		row[:architecture_definition_cost].scan(/(\d+.\d+) for/).flatten.each { |slice| hours += slice.to_f }
		components[row[:component_name]][row[:t].to_f]["architecture_definition_cost"] = hours
	end
	if row[:architecture_evaluation_cost] != ""
		hours = 0.0
		row[:architecture_evaluation_cost].scan(/(\d+.\d+) [^\d]+ at (\d+.\d+)/).flatten.each_slice(2) { |slice| hours += slice[0].to_f * slice[1].to_f}
		row[:architecture_evaluation_cost].scan(/(\d+.\d+) for/).flatten.each { |slice| hours += slice.to_f }
		components[row[:component_name]][row[:t].to_f]["architecture_evaluation_cost"] = hours
	end
	if row[:component_development_cost] != ""
		hours = 0.0
		row[:component_development_cost].scan(/(\d+.\d+) [^\d]+ at (\d+.\d+)/).flatten.each_slice(2) { |slice| hours += slice[0].to_f * slice[1].to_f}
		row[:component_development_cost].scan(/(\d+.\d+) for/).flatten.each { |slice| hours += slice.to_f }
		components[row[:component_name]][row[:t].to_f]["component_development_cost"] = hours
	end
	if row[:mining_existing_assets_cost] != ""
		hours = 0.0
		row[:mining_existing_assets_cost].scan(/(\d+.\d+) [^\d]+ at (\d+.\d+)/).flatten.each_slice(2) { |slice| hours += slice[0].to_f * slice[1].to_f}
		row[:mining_existing_assets_cost].scan(/(\d+.\d+) for/).flatten.each { |slice| hours += slice.to_f }
		components[row[:component_name]][row[:t].to_f]["mining_existing_assets_cost"] = hours
	end
	if row[:requirements_engineering_cost] != ""
		hours = 0.0
		row[:requirements_engineering_cost].scan(/(\d+.\d+) [^\d]+ at (\d+.\d+)/).flatten.each_slice(2) { |slice| hours += slice[0].to_f * slice[1].to_f}
		row[:requirements_engineering_cost].scan(/(\d+.\d+) for/).flatten.each { |slice| hours += slice.to_f }
		components[row[:component_name]][row[:t].to_f]["requirements_engineering_cost"] = hours
	end
	if row[:software_system_integration_cost] != ""
		hours = 0.0
		row[:software_system_integration_cost].scan(/(\d+.\d+) [^\d]+ at (\d+.\d+)/).flatten.each_slice(2) { |slice| hours += slice[0].to_f * slice[1].to_f}
		row[:software_system_integration_cost].scan(/(\d+.\d+) for/).flatten.each { |slice| hours += slice.to_f }
		components[row[:component_name]][row[:t].to_f]["software_system_integration_cost"] = hours
	end
	if row[:testing_cost] != ""
		hours = 0.0
		row[:testing_cost].scan(/(\d+.\d+) [^\d]+ at (\d+.\d+)/).flatten.each_slice(2) { |slice| hours += slice[0].to_f * slice[1].to_f}
		row[:testing_cost].scan(/(\d+.\d+) for/).flatten.each { |slice| hours += slice.to_f }
		components[row[:component_name]][row[:t].to_f]["testing_cost"] = hours
	end
	if row[:understanding_relevant_domains_cost] != ""
		hours = 0.0
		row[:understanding_relevant_domains_cost].scan(/(\d+.\d+) [^\d]+ at (\d+.\d+)/).flatten.each_slice(2) { |slice| hours += slice[0].to_f * slice[1].to_f}
		row[:understanding_relevant_domains_cost].scan(/(\d+.\d+) for/).flatten.each { |slice| hours += slice.to_f }
		components[row[:component_name]][row[:t].to_f]["understanding_relevant_domains_cost"] = hours
	end
	if row[:using_externally_available_software_cost] != ""
		hours = 0.0
		row[:using_externally_available_software_cost].scan(/(\d+.\d+) [^\d]+ at (\d+.\d+)/).flatten.each_slice(2) { |slice| hours += slice[0].to_f * slice[1].to_f}
		row[:using_externally_available_software_cost].scan(/(\d+.\d+) for/).flatten.each { |slice| hours += slice.to_f }
		components[row[:component_name]][row[:t].to_f]["using_externally_available_software_cost"] = hours
	end
	if row[:configuration_management_cost] != ""
		hours = 0.0
		row[:configuration_management_cost].scan(/(\d+.\d+) [^\d]+ at (\d+.\d+)/).flatten.each_slice(2) { |slice| hours += slice[0].to_f * slice[1].to_f}
		row[:configuration_management_cost].scan(/(\d+.\d+) for/).flatten.each { |slice| hours += slice.to_f }
		components[row[:component_name]][row[:t].to_f]["configuration_management_cost"] = hours
	end
	if row[:commission_analysis_cost] != ""
		hours = 0.0
		row[:commission_analysis_cost].scan(/(\d+.\d+) [^\d]+ at (\d+.\d+)/).flatten.each_slice(2) { |slice| hours += slice[0].to_f * slice[1].to_f}
		row[:commission_analysis_cost].scan(/(\d+.\d+) for/).flatten.each { |slice| hours += slice.to_f }
		components[row[:component_name]][row[:t].to_f]["commission_analysis_cost"] = hours
	end
	if row[:measurement_tracking_cost] != ""
		hours = 0.0
		row[:measurement_tracking_cost].scan(/(\d+.\d+) [^\d]+ at (\d+.\d+)/).flatten.each_slice(2) { |slice| hours += slice[0].to_f * slice[1].to_f}
		row[:measurement_tracking_cost].scan(/(\d+.\d+) for/).flatten.each { |slice| hours += slice.to_f }
		components[row[:component_name]][row[:t].to_f]["measurement_tracking_cost"] = hours
	end
	if row[:process_discipline_cost] != ""
		hours = 0.0
		row[:process_discipline_cost].scan(/(\d+.\d+) [^\d]+ at (\d+.\d+)/).flatten.each_slice(2) { |slice| hours += slice[0].to_f * slice[1].to_f}
		row[:process_discipline_cost].scan(/(\d+.\d+) for/).flatten.each { |slice| hours += slice.to_f }
		components[row[:component_name]][row[:t].to_f]["process_discipline_cost"] = hours
	end
	if row[:scoping_cost] != ""
		hours = 0.0
		row[:scoping_cost].scan(/(\d+.\d+) [^\d]+ at (\d+.\d+)/).flatten.each_slice(2) { |slice| hours += slice[0].to_f * slice[1].to_f}
		row[:scoping_cost].scan(/(\d+.\d+) for/).flatten.each { |slice| hours += slice.to_f }
		components[row[:component_name]][row[:t].to_f]["scoping_cost"] = hours
	end
	if row[:technical_planning_cost] != ""
		hours = 0.0
		row[:technical_planning_cost].scan(/(\d+.\d+) [^\d]+ at (\d+.\d+)/).flatten.each_slice(2) { |slice| hours += slice[0].to_f * slice[1].to_f}
		row[:technical_planning_cost].scan(/(\d+.\d+) for/).flatten.each { |slice| hours += slice.to_f }
		components[row[:component_name]][row[:t].to_f]["technical_planning_cost"] = hours
	end
	if row[:technical_risk_management_cost] != ""
		hours = 0.0
		row[:technical_risk_management_cost].scan(/(\d+.\d+) [^\d]+ at (\d+.\d+)/).flatten.each_slice(2) { |slice| hours += slice[0].to_f * slice[1].to_f}
		row[:technical_risk_management_cost].scan(/(\d+.\d+) for/).flatten.each { |slice| hours += slice.to_f }
		components[row[:component_name]][row[:t].to_f]["technical_risk_management_cost"] = hours
	end
	if row[:tool_support_cost] != ""
		hours = 0.0
		row[:tool_support_cost].scan(/(\d+.\d+) [^\d]+ at (\d+.\d+)/).flatten.each_slice(2) { |slice| hours += slice[0].to_f * slice[1].to_f}
		row[:tool_support_cost].scan(/(\d+.\d+) for/).flatten.each { |slice| hours += slice.to_f }
		components[row[:component_name]][row[:t].to_f]["tool_support_cost"] = hours
	end
	if row[:building_business_case_cost] != ""
		hours = 0.0
		row[:building_business_case_cost].scan(/(\d+.\d+) [^\d]+ at (\d+.\d+)/).flatten.each_slice(2) { |slice| hours += slice[0].to_f * slice[1].to_f}
		row[:building_business_case_cost].scan(/(\d+.\d+) for/).flatten.each { |slice| hours += slice.to_f }
		components[row[:component_name]][row[:t].to_f]["building_business_case_cost"] = hours
	end
	if row[:customer_interface_management_cost] != ""
		hours = 0.0
		row[:customer_interface_management_cost].scan(/(\d+.\d+) [^\d]+ at (\d+.\d+)/).flatten.each_slice(2) { |slice| hours += slice[0].to_f * slice[1].to_f}
		row[:customer_interface_management_cost].scan(/(\d+.\d+) for/).flatten.each { |slice| hours += slice.to_f }
		components[row[:component_name]][row[:t].to_f]["customer_interface_management_cost"] = hours
	end
	if row[:developing_acquisition_strategy_cost] != ""
		hours = 0.0
		row[:developing_acquisition_strategy_cost].scan(/(\d+.\d+) [^\d]+ at (\d+.\d+)/).flatten.each_slice(2) { |slice| hours += slice[0].to_f * slice[1].to_f}
		row[:developing_acquisition_strategy_cost].scan(/(\d+.\d+) for/).flatten.each { |slice| hours += slice.to_f }
		components[row[:component_name]][row[:t].to_f]["developing_acquisition_strategy_cost"] = hours
	end
	if row[:funding_cost] != ""
		hours = 0.0
		row[:funding_cost].scan(/(\d+.\d+) [^\d]+ at (\d+.\d+)/).flatten.each_slice(2) { |slice| hours += slice[0].to_f * slice[1].to_f}
		row[:funding_cost].scan(/(\d+.\d+) for/).flatten.each { |slice| hours += slice.to_f }
		components[row[:component_name]][row[:t].to_f]["funding_cost"] = hours
	end
	if row[:launching_institutionalizing_cost] != ""
		hours = 0.0
		row[:launching_institutionalizing_cost].scan(/(\d+.\d+) [^\d]+ at (\d+.\d+)/).flatten.each_slice(2) { |slice| hours += slice[0].to_f * slice[1].to_f}
		row[:launching_institutionalizing_cost].scan(/(\d+.\d+) for/).flatten.each { |slice| hours += slice.to_f }
		components[row[:component_name]][row[:t].to_f]["launching_institutionalizing_cost"] = hours
	end
	if row[:market_analysis_cost] != ""
		hours = 0.0
		row[:market_analysis_cost].scan(/(\d+.\d+) [^\d]+ at (\d+.\d+)/).flatten.each_slice(2) { |slice| hours += slice[0].to_f * slice[1].to_f}
		row[:market_analysis_cost].scan(/(\d+.\d+) for/).flatten.each { |slice| hours += slice.to_f }
		components[row[:component_name]][row[:t].to_f]["market_analysis_cost"] = hours
	end
	if row[:operations_cost] != ""
		hours = 0.0
		row[:operations_cost].scan(/(\d+.\d+) [^\d]+ at (\d+.\d+)/).flatten.each_slice(2) { |slice| hours += slice[0].to_f * slice[1].to_f}
		row[:operations_cost].scan(/(\d+.\d+) for/).flatten.each { |slice| hours += slice.to_f }
		components[row[:component_name]][row[:t].to_f]["operations_cost"] = hours
	end
	if row[:organizational_planning_cost] != ""
		hours = 0.0
		row[:organizational_planning_cost].scan(/(\d+.\d+) [^\d]+ at (\d+.\d+)/).flatten.each_slice(2) { |slice| hours += slice[0].to_f * slice[1].to_f}
		row[:organizational_planning_cost].scan(/(\d+.\d+) for/).flatten.each { |slice| hours += slice.to_f }
		components[row[:component_name]][row[:t].to_f]["organizational_planning_cost"] = hours
	end
	if row[:organizational_risk_management_cost] != ""
		hours = 0.0
		row[:organizational_risk_management_cost].scan(/(\d+.\d+) [^\d]+ at (\d+.\d+)/).flatten.each_slice(2) { |slice| hours += slice[0].to_f * slice[1].to_f}
		row[:organizational_risk_management_cost].scan(/(\d+.\d+) for/).flatten.each { |slice| hours += slice.to_f }
		components[row[:component_name]][row[:t].to_f]["organizational_risk_management_cost"] = hours
	end
	if row[:structuring_organization_cost] != ""
		hours = 0.0
		row[:structuring_organization_cost].scan(/(\d+.\d+) [^\d]+ at (\d+.\d+)/).flatten.each_slice(2) { |slice| hours += slice[0].to_f * slice[1].to_f}
		row[:structuring_organization_cost].scan(/(\d+.\d+) for/).flatten.each { |slice| hours += slice.to_f }
		components[row[:component_name]][row[:t].to_f]["structuring_organization_cost"] = hours
	end
	if row[:technology_forecasting_cost] != ""
		hours = 0.0
		row[:technology_forecasting_cost].scan(/(\d+.\d+) [^\d]+ at (\d+.\d+)/).flatten.each_slice(2) { |slice| hours += slice[0].to_f * slice[1].to_f}
		row[:technology_forecasting_cost].scan(/(\d+.\d+) for/).flatten.each { |slice| hours += slice.to_f }
		components[row[:component_name]][row[:t].to_f]["technology_forecasting_cost"] = hours
	end
	if row[:training_cost] != ""
		hours = 0.0
		row[:training_cost].scan(/(\d+.\d+) [^\d]+ at (\d+.\d+)/).flatten.each_slice(2) { |slice| hours += slice[0].to_f * slice[1].to_f}
		row[:training_cost].scan(/(\d+.\d+) for/).flatten.each { |slice| hours += slice.to_f }
		components[row[:component_name]][row[:t].to_f]["training_cost"] = hours
	end
end

components.keys.each do |component_name|
	puts "Plotly Bar Graph for #{component_name}"

	architecture_definition_cost = []
	architecture_evaluation_cost = []
	component_development_cost = []
	mining_existing_assets_cost = []
	requirements_engineering_cost = []
	software_system_integration_cost = []
	testing_cost = []
	understanding_relevant_domains_cost = []
	using_externally_available_software_cost = []
	configuration_management_cost = []
	commission_analysis_cost = []
	measurement_tracking_cost = []
	process_discipline_cost = []
	scoping_cost = []
	technical_planning_cost = []
	technical_risk_management_cost = []
	tool_support_cost = []
	building_business_case_cost = []
	customer_interface_management_cost = []
	developing_acquisition_strategy_cost = []
	funding_cost = []
	launching_institutionalizing_cost = []
	market_analysis_cost = []
	operations_cost = []
	organizational_planning_cost = []
	organizational_risk_management_cost = []
	structuring_organization_cost = []
	technology_forecasting_cost = []
	training_cost = []

	component = components[component_name]
	time_periods = component.keys
	puts "time_periods <- c(#{time_periods.collect {|x| "#{x}"}.join(",")})"
	time_periods.each do |period|
		component_at_t = component[period]
		if component_at_t["architecture_definition_cost"]
			architecture_definition_cost << component_at_t["architecture_definition_cost"]
		else
			architecture_definition_cost << 0.0
		end
		if component_at_t["architecture_evaluation_cost"]
			architecture_evaluation_cost << component_at_t["architecture_evaluation_cost"]
		else
			architecture_evaluation_cost << 0.0
		end
		if component_at_t["component_development_cost"]
			component_development_cost << component_at_t["component_development_cost"]
		else
			component_development_cost << 0.0
		end
		if component_at_t["mining_existing_assets_cost"]
			mining_existing_assets_cost << component_at_t["mining_existing_assets_cost"]
		else
			mining_existing_assets_cost << 0.0
		end
		if component_at_t["requirements_engineering_cost"]
			requirements_engineering_cost << component_at_t["requirements_engineering_cost"]
		else
			requirements_engineering_cost << 0.0
		end
		if component_at_t["software_system_integration_cost"]
			software_system_integration_cost << component_at_t["software_system_integration_cost"]
		else
			software_system_integration_cost << 0.0
		end
		if component_at_t["testing_cost"]
			testing_cost << component_at_t["testing_cost"]
		else
			testing_cost << 0.0
		end
		if component_at_t["understanding_relevant_domains_cost"]
			understanding_relevant_domains_cost << component_at_t["understanding_relevant_domains_cost"]
		else
			understanding_relevant_domains_cost << 0.0
		end
		if component_at_t["using_externally_available_software_cost"]
			using_externally_available_software_cost << component_at_t["using_externally_available_software_cost"]
		else
			using_externally_available_software_cost << 0.0
		end
		if component_at_t["configuration_management_cost"]
			configuration_management_cost << component_at_t["configuration_management_cost"]
		else
			configuration_management_cost << 0.0
		end
		if component_at_t["commission_analysis_cost"]
			commission_analysis_cost << component_at_t["commission_analysis_cost"]
		else
			commission_analysis_cost << 0.0
		end
		if component_at_t["measurement_tracking_cost"]
			measurement_tracking_cost << component_at_t["measurement_tracking_cost"]
		else
			measurement_tracking_cost << 0.0
		end
		if component_at_t["process_discipline_cost"]
			process_discipline_cost << component_at_t["process_discipline_cost"]
		else
			process_discipline_cost << 0.0
		end
		if component_at_t["scoping_cost"]
			scoping_cost << component_at_t["scoping_cost"]
		else
			scoping_cost << 0.0
		end
		if component_at_t["technical_planning_cost"]
			technical_planning_cost << component_at_t["technical_planning_cost"]
		else
			technical_planning_cost << 0.0
		end
		if component_at_t["technical_risk_management_cost"]
			technical_risk_management_cost << component_at_t["technical_risk_management_cost"]
		else
			technical_risk_management_cost << 0.0
		end
		if component_at_t["tool_support_cost"]
			tool_support_cost << component_at_t["tool_support_cost"]
		else
			tool_support_cost << 0.0
		end
		if component_at_t["building_business_case_cost"]
			building_business_case_cost << component_at_t["building_business_case_cost"]
		else
			building_business_case_cost << 0.0
		end
		if component_at_t["customer_interface_management_cost"]
			customer_interface_management_cost << component_at_t["customer_interface_management_cost"]
		else
			customer_interface_management_cost << 0.0
		end
		if component_at_t["developing_acquisition_strategy_cost"]
			developing_acquisition_strategy_cost << component_at_t["developing_acquisition_strategy_cost"]
		else
			developing_acquisition_strategy_cost << 0.0
		end
		if component_at_t["funding_cost"]
			funding_cost << component_at_t["funding_cost"]
		else
			funding_cost << 0.0
		end
		if component_at_t["launching_institutionalizing_cost"]
			launching_institutionalizing_cost << component_at_t["launching_institutionalizing_cost"]
		else
			launching_institutionalizing_cost << 0.0
		end
		if component_at_t["market_analysis_cost"]
			market_analysis_cost << component_at_t["market_analysis_cost"]
		else
			market_analysis_cost << 0.0
		end
		if component_at_t["operations_cost"]
			operations_cost << component_at_t["operations_cost"]
		else
			operations_cost << 0.0
		end
		if component_at_t["organizational_planning_cost"]
			organizational_planning_cost << component_at_t["organizational_planning_cost"]
		else
			organizational_planning_cost << 0.0
		end
		if component_at_t["organizational_risk_management_cost"]
			organizational_risk_management_cost << component_at_t["organizational_risk_management_cost"]
		else
			organizational_risk_management_cost << 0.0
		end
		if component_at_t["structuring_organization_cost"]
			structuring_organization_cost << component_at_t["structuring_organization_cost"]
		else
			structuring_organization_cost << 0.0
		end
		if component_at_t["technology_forecasting_cost"]
			technology_forecasting_cost << component_at_t["technology_forecasting_cost"]
		else
			technology_forecasting_cost << 0.0
		end
		if component_at_t["training_cost"]
			training_cost << component_at_t["training_cost"]
		else
			training_cost << 0.0
		end
	end

	data_frame = []
	if architecture_definition_cost.inject(0, :+) > 0.0
		data_frame << "architecture_definition_cost"
		puts "architecture_definition_cost <- c(#{architecture_definition_cost.join(",")})"
	end
	if architecture_evaluation_cost.inject(0, :+) > 0.0
		data_frame << "architecture_evaluation_cost"
		puts "architecture_evaluation_cost <- c(#{architecture_evaluation_cost.join(",")})"
	end
	if component_development_cost.inject(0, :+) > 0.0
		data_frame << "component_development_cost"
		puts "component_development_cost <- c(#{component_development_cost.join(",")})"
	end
	if mining_existing_assets_cost.inject(0, :+) > 0.0
		data_frame << "mining_existing_assets_cost"
		puts "mining_existing_assets_cost <- c(#{mining_existing_assets_cost.join(",")})"
	end
	if requirements_engineering_cost.inject(0, :+) > 0.0
		data_frame << "requirements_engineering_cost"
		puts "requirements_engineering_cost <- c(#{requirements_engineering_cost.join(",")})"
	end
	if software_system_integration_cost.inject(0, :+) > 0.0
		data_frame << "software_system_integration_cost"
		puts "software_system_integration_cost <- c(#{software_system_integration_cost.join(",")})"
	end
	if testing_cost.inject(0, :+) > 0.0
		data_frame << "testing_cost"
		puts "testing_cost <- c(#{testing_cost.join(",")})"
	end
	if understanding_relevant_domains_cost.inject(0, :+) > 0.0
		data_frame << "understanding_relevant_domains_cost"
		puts "understanding_relevant_domains_cost <- c(#{understanding_relevant_domains_cost.join(",")})"
	end
	if using_externally_available_software_cost.inject(0, :+) > 0.0
		data_frame << "using_externally_available_software_cost"
		puts "using_externally_available_software_cost <- c(#{using_externally_available_software_cost.join(",")})"
	end
	if configuration_management_cost.inject(0, :+) > 0.0
		data_frame << "configuration_management_cost"
		puts "configuration_management_cost <- c(#{configuration_management_cost.join(",")})"
	end
	if commission_analysis_cost.inject(0, :+) > 0.0
		data_frame << "commission_analysis_cost"
		puts "commission_analysis_cost <- c(#{commission_analysis_cost.join(",")})"
	end
	if measurement_tracking_cost.inject(0, :+) > 0.0
		data_frame << "measurement_tracking_cost"
		puts "measurement_tracking_cost <- c(#{measurement_tracking_cost.join(",")})"
	end
	if process_discipline_cost.inject(0, :+) > 0.0
		data_frame << "process_discipline_cost"
		puts "process_discipline_cost <- c(#{process_discipline_cost.join(",")})"
	end
	if scoping_cost.inject(0, :+) > 0.0
		data_frame << "scoping_cost"
		puts "scoping_cost <- c(#{scoping_cost.join(",")})"
	end
	if technical_planning_cost.inject(0, :+) > 0.0
		data_frame << "technical_planning_cost"
		puts "technical_planning_cost <- c(#{technical_planning_cost.join(",")})"
	end
	if technical_risk_management_cost.inject(0, :+) > 0.0
		data_frame << "technical_risk_management_cost"
		puts "technical_risk_management_cost <- c(#{technical_risk_management_cost.join(",")})"
	end
	if tool_support_cost.inject(0, :+) > 0.0
		data_frame << "tool_support_cost"
		puts "tool_support_cost <- c(#{tool_support_cost.join(",")})"
	end
	if building_business_case_cost.inject(0, :+) > 0.0
		data_frame << "building_business_case_cost"
		puts "building_business_case_cost <- c(#{building_business_case_cost.join(",")})"
	end
	if customer_interface_management_cost.inject(0, :+) > 0.0
		data_frame << "customer_interface_management_cost"
		puts "customer_interface_management_cost <- c(#{customer_interface_management_cost.join(",")})"
	end
	if developing_acquisition_strategy_cost.inject(0, :+) > 0.0
		data_frame << "developing_acquisition_strategy_cost"
		puts "developing_acquisition_strategy_cost <- c(#{developing_acquisition_strategy_cost.join(",")})"
	end
	if funding_cost.inject(0, :+) > 0.0
		data_frame << "funding_cost"
		puts "funding_cost <- c(#{funding_cost.join(",")})"
	end
	if launching_institutionalizing_cost.inject(0, :+) > 0.0
		data_frame << "launching_institutionalizing_cost"
		puts "launching_institutionalizing_cost <- c(#{launching_institutionalizing_cost.join(",")})"
	end
	if market_analysis_cost.inject(0, :+) > 0.0
		data_frame << "market_analysis_cost"
		puts "market_analysis_cost <- c(#{market_analysis_cost.join(",")})"
	end
	if operations_cost.inject(0, :+) > 0.0
		data_frame << "operations_cost"
		puts "operations_cost <- c(#{operations_cost.join(",")})"
	end
	if organizational_planning_cost.inject(0, :+) > 0.0
		data_frame << "organizational_planning_cost"
		puts "organizational_planning_cost <- c(#{organizational_planning_cost.join(",")})"
	end
	if organizational_risk_management_cost.inject(0, :+) > 0.0
		data_frame << "organizational_risk_management_cost"
		puts "organizational_risk_management_cost <- c(#{organizational_risk_management_cost.join(",")})"
	end
	if structuring_organization_cost.inject(0, :+) > 0.0
		data_frame << "structuring_organization_cost"
		puts "structuring_organization_cost <- c(#{structuring_organization_cost.join(",")})"
	end
	if technology_forecasting_cost.inject(0, :+) > 0.0
		data_frame << "technology_forecasting_cost"
		puts "technology_forecasting_cost <- c(#{technology_forecasting_cost.join(",")})"
	end
	if training_cost.inject(0, :+) > 0.0
		data_frame << "training_cost"
		puts "training_cost <- c(#{training_cost.join(",")})"
	end
	puts
	puts "data <- data.frame(#{data_frame.join(",")})"
	puts "p <- plot_ly(data, x = ~time_periods, y = ~#{data_frame[0]}, type = 'bar', name = '#{data_frame[0].gsub("_", " ").gsub("cost", "hours").split.map(&:capitalize).join(' ')}') %>%"
	(2..data_frame.size).each do |i|
  		puts "add_trace(y = ~#{data_frame[i-1]}, name = '#{data_frame[i-1].gsub("_", " ").gsub("cost", "hours").split.map(&:capitalize).join(' ')}') %>%"
	end
	puts "layout(xaxis = list(title = 'Time Periods'), yaxis = list(title = 'Hours Required'), barmode = 'group')"
  	puts "embed_notebook(p)"
end