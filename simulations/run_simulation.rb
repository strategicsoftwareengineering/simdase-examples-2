def run_simulation(simulation, iterations)
	iterations.each do |iteration|
		`rm simulation-#{simulation}/#{iteration}.csv`
		`cd simulation-#{simulation} && ruby run_simulation.rb #{iteration} > #{iteration}.csv && cd ..`
	end
end

print "Which simulation would you like to run (1 | 2 | 3 | A): "
simulation = gets.chomp
print "How many iterations (for multiple iterations, enter a comma separated list): "
iterations = gets.chomp.split(",").collect { |x| x.strip.to_i }
if simulation.downcase == "a"
	run_simulation(1, iterations)
	run_simulation(2, iterations)
	run_simulation(3, iterations)
else
	if simulation == "1"
		run_simulation(1, iterations)
	elsif simulation == "2"
		run_simulation(2, iterations)
	elsif simulation == "3"
		run_simulation(3, iterations)
	else
		puts "Invalid Choice: Please enter 1, 2, 3 or A"
	end
end
