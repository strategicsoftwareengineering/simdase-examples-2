package entire_system
public
	with Base_Types, national, common;
	
	abstract producers
		features
			vegetables_produced: out data port Base_Types::Integer;
			bread_produced: out data port Base_Types::Integer;
			dairy_produced: out data port Base_Types::Integer;
			meat_produced: out data port Base_Types::Integer;
	end producers;
	
	abstract implementation producers.impl
		subcomponents
			national_gatherer: abstract common::producer_gatherer.impl;
		connections
			conn_1: port national_gatherer.total_vegetables_produced -> vegetables_produced;
			conn_2: port national_gatherer.total_bread_produced -> bread_produced;
			conn_3: port national_gatherer.total_dairy_produced -> dairy_produced;
			conn_4: port national_gatherer.total_meat_produced -> meat_produced; 
	end producers.impl;
	
	abstract consumers
		features
			vegetables_received: in data port Base_Types::Integer;
			bread_received: in data port Base_Types::Integer;
			dairy_received: in data port Base_Types::Integer;
			meat_received: in data port Base_Types::Integer;
	end consumers;
	
	abstract implementation consumers.impl
		subcomponents
			national_consumers: abstract national::national_consumers.impl;
		connections
			conn_1: port vegetables_received -> national_consumers.vegetables_received;
			conn_2: port bread_received -> national_consumers.bread_received;
			conn_3: port dairy_received -> national_consumers.dairy_received;
			conn_4: port meat_received -> national_consumers.meat_received;
	end consumers.impl;
	
	system entire_system
	end entire_system;
	
	system implementation entire_system.impl
		subcomponents
			producers: abstract producers.impl;
			consumers: abstract consumers.impl;
		connections
			conn_1: port producers.vegetables_produced -> consumers.vegetables_received;
			conn_2: port producers.bread_produced -> consumers.bread_received;
			conn_3: port producers.dairy_produced -> consumers.dairy_received;
			conn_4: port producers.meat_produced -> consumers.meat_received;
		annex simdase {**
			time_min => 1.0;
			time_max => 78.0; --(each time period is one week)
		**};
	end entire_system.impl;
	
end entire_system;
