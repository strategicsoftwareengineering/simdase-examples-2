package fork_lift_tracker
public
	with fork_lift_tracking_system, Base_Types;
	
	-- devices
	
	device rfid_reader
		features
			x: out data port Base_Types::Float_64;
			y: out data port Base_Types::Float_64;
			tag_id: out data port Base_Types::String;
			error_margin: out data port Base_Types::Float_64;
	end rfid_reader;
	
	device implementation rfid_reader.impl
	end rfid_reader.impl;
	
	abstract communication_module
		features
			power_on: in event port;
			power_off: in event port;
			signal_strength: out data port Base_Types::Integer;
			send: in event data port fork_lift_tracking_system::tracking_information;
			data_send_event: out event data port fork_lift_tracking_system::tracking_information;
	end communication_module;
	
	device wireless_module extends communication_module
	end wireless_module;
	
	device implementation wireless_module.impl
	end wireless_module.impl;
	
	device bluetooth_module extends communication_module
	end bluetooth_module;
	
	device implementation bluetooth_module.impl
	end bluetooth_module.impl;
	
	device radio_module extends communication_module
	end radio_module;
	
	device implementation radio_module.impl
	end radio_module.impl;
	
	-- threads
	
	thread poll_positioning_tag
		features
			coordinates: requires data access fork_lift_tracking_system::coordinates;
			
			x: in data port Base_Types::Float_64;
			y: in data port Base_Types::Float_64;
			tag_id: in data port Base_Types::String;
			error_margin: in data port Base_Types::Float_64;
			
			tracking_info: out event data port fork_lift_tracking_system::tracking_information;
	end poll_positioning_tag;
	
	thread implementation poll_positioning_tag.impl
		properties
			Dispatch_Protocol => Periodic;
			Period => 5000 ms;
		annex behavior_specification {**
			variables
				previous_location: fork_lift_tracking_system::coordinates;
				current_location: fork_lift_tracking_system::coordinates;
				speed: Base_Types::Float_64;
				tracking_information: fork_lift_tracking_system::tracking_information;
			states
				start: initial state;
				offline: complete state;
				online: state;
			transitions
				start -[]-> offline;
				offline -[on dispatch]-> online {
					previous_location := current_location;
					coordinates.build_coordinates!(x, y, tag_id, error_margin, current_location);
					coordinates.compute_speed!(speed);
					coordinates.build_tracking_information!(current_location, speed, tracking_information);
					tracking_info!(tracking_information)
				};
				online -[]-> offline;
		**};
	end poll_positioning_tag.impl;
	
	thread send_location_and_speed
		features
			tracking_information: requires data access fork_lift_tracking_system::tracking_information;
			tracking_info_event: in event data port fork_lift_tracking_system::tracking_information;
			
			send_method: out event data port fork_lift_tracking_system::tracking_information;
	end send_location_and_speed;
	
	thread implementation send_location_and_speed.impl
		properties
			Dispatch_Protocol => APeriodic;
		annex behavior_specification {**
			variables
				tracking_info: fork_lift_tracking_system::tracking_information;
			states
				start: initial state;
				offline: complete state;
				online: state;
			transitions
				offline -[on dispatch tracking_info_event]-> online {
					 tracking_info_event?(tracking_info)
				};
				online -[]-> offline {
					send_method!(tracking_info)
				};
		**};
	end send_location_and_speed.impl;
	
	thread monitor_signal_and_location
		features
			is_enabled_at_location_interface: requires data access fork_lift_tracking_system::is_enabled_at_location_interface;
			tracking_information: requires data access fork_lift_tracking_system::tracking_information;
			
			tracking_info_event: in event data port fork_lift_tracking_system::tracking_information;
			
			wireless_power_on: out event port;
			wireless_power_off: out event port;
			wireless_signal_strength: in data port Base_Types::Integer;
			
			bluetooth_power_on: out event port;
			bluetooth_power_off: out event port;
			bluetooth_signal_strength: in data port Base_Types::Integer;
			
			radio_power_on: out event port;
			radio_power_off: out event port;
			radio_signal_strength: in data port Base_Types::Integer;
			
			use_wifi: out event port;
			use_ble: out event port;
			use_radio: out event port;
	end monitor_signal_and_location;
	
	thread implementation monitor_signal_and_location.impl
		annex behavior_specification {**
			variables
				tracking_info: fork_lift_tracking_system::tracking_information;
				location: fork_lift_tracking_system::coordinates;
				wireless_enabled: Base_Types::Boolean;
				bluetooth_enabled: Base_Types::Boolean;
				radio_enabled: Base_Types::Boolean;
				wireless_rating: Base_Types::Integer;
				bluetooth_rating: Base_Types::Integer;
				radio_rating: Base_Types::Integer;
			states
				start: initial state;
				offline: complete state;
				online: state;
			transitions
				offline -[on dispatch tracking_info_event]-> online {
					 tracking_info_event?(tracking_info)
				};
				online -[]-> offline {
					tracking_information.get_location!(tracking_info, location);
					is_enabled_at_location_interface.is_wireless_enabled_at_location!(location, wireless_enabled);
					is_enabled_at_location_interface.is_bluetooth_enabled_at_location!(location, bluetooth_enabled);
					is_enabled_at_location_interface.is_radio_enabled_at_location!(location, radio_enabled);
					if (wireless_enabled)
						wireless_power_on!;
						wireless_rating := wireless_signal_strength
					else
						wireless_power_off!;
						wireless_rating := 0
					end if;
					if (bluetooth_enabled)
						bluetooth_power_on!;
						bluetooth_rating := bluetooth_signal_strength
					else
						bluetooth_power_off!;
						bluetooth_rating := 0
					end if;
					if (radio_enabled)
						radio_power_on!;
						radio_rating := radio_signal_strength
					else
						radio_power_off!;
						radio_rating := 0
					end if;
					if (wireless_rating > bluetooth_rating)
						if (wireless_rating > radio_rating)
							use_wifi!
						end if
					end if;
					if (bluetooth_rating > wireless_rating)
						if (bluetooth_rating > radio_rating)
							use_ble!
						end if
					end if;
					if (radio_rating > bluetooth_rating)
						if (radio_rating > wireless_rating)
							use_radio!
						end if
					end if
				};
		**};
	end monitor_signal_and_location.impl;
	
	-- process
	
	process send_tracking_info
		features
			coordinates: requires data access fork_lift_tracking_system::coordinates;
			tracking_information: requires data access fork_lift_tracking_system::tracking_information;
			is_enabled_at_location_interface: requires data access fork_lift_tracking_system::is_enabled_at_location_interface;
			
			x: in data port Base_Types::Float_64;
			y: in data port Base_Types::Float_64;
			tag_id: in data port Base_Types::String;
			error_margin: in data port Base_Types::Float_64;
			
			send_method: out event data port fork_lift_tracking_system::tracking_information;
	end send_tracking_info;
	
	process implementation send_tracking_info.impl
		subcomponents
			polling_thread: thread poll_positioning_tag.impl;
			send_thread: thread send_location_and_speed.impl;
		connections
			data_access_1: data access coordinates -> polling_thread.coordinates;
			data_access_2: data access tracking_information -> send_thread.tracking_information;
			
			conn_1: port polling_thread.tracking_info -> send_thread.tracking_info_event;
			conn_2: port x -> polling_thread.x;
			conn_3: port y -> polling_thread.y;
			conn_4: port tag_id -> polling_thread.tag_id;
			conn_5: port error_margin -> polling_thread.error_margin;
			conn_6: port send_thread.send_method -> send_method;
	end send_tracking_info.impl;
	
	process protocol_management_process
		features
			is_enabled_at_location_interface: requires data access fork_lift_tracking_system::is_enabled_at_location_interface;
			tracking_information: requires data access fork_lift_tracking_system::tracking_information;
			
			tracking_info_event: in event data port fork_lift_tracking_system::tracking_information;
			
			wireless_power_on: out event port;
			wireless_power_off: out event port;
			wireless_signal_strength: in data port Base_Types::Integer;
			
			bluetooth_power_on: out event port;
			bluetooth_power_off: out event port;
			bluetooth_signal_strength: in data port Base_Types::Integer;
			
			radio_power_on: out event port;
			radio_power_off: out event port;
			radio_signal_strength: in data port Base_Types::Integer;
			
			use_wifi: out event port;
			use_ble: out event port;
			use_radio: out event port;
	end protocol_management_process;
	
	process implementation protocol_management_process.impl
		subcomponents
			monitor_signal_and_location: thread monitor_signal_and_location.impl;
		connections
			data_conn_1: data access is_enabled_at_location_interface -> monitor_signal_and_location.is_enabled_at_location_interface;
			data_conn_2: data access tracking_information -> monitor_signal_and_location.tracking_information;
			
			conn_1: port tracking_info_event -> monitor_signal_and_location.tracking_info_event;
			conn_2: port monitor_signal_and_location.wireless_power_on -> wireless_power_on;
			conn_3: port monitor_signal_and_location.wireless_power_off -> wireless_power_off;
			conn_4: port wireless_signal_strength -> monitor_signal_and_location.wireless_signal_strength;
			conn_5: port monitor_signal_and_location.bluetooth_power_on -> bluetooth_power_on;
			conn_6: port monitor_signal_and_location.bluetooth_power_off -> bluetooth_power_off;
			conn_7: port bluetooth_signal_strength -> monitor_signal_and_location.bluetooth_signal_strength;
			conn_8: port monitor_signal_and_location.radio_power_on -> radio_power_on;
			conn_9: port monitor_signal_and_location.radio_power_off -> radio_power_off;
			conn_10: port radio_signal_strength -> monitor_signal_and_location.radio_signal_strength;
			conn_11: port monitor_signal_and_location.use_wifi -> use_wifi; 
			conn_12: port monitor_signal_and_location.use_ble -> use_ble; 
			conn_13: port monitor_signal_and_location.use_radio -> use_radio;
	end protocol_management_process.impl;
	
	-- system
	
	system fork_lift_tracker
		features
			wifi_tracking_information: out event data port fork_lift_tracking_system::tracking_information;
			bluetooth_tracking_information: out event data port fork_lift_tracking_system::tracking_information;
			radio_tracking_information: out event data port fork_lift_tracking_system::tracking_information;
		modes
			wifi_enabled: initial mode;
			ble_enabled: mode;
			radio_enabled: mode;
	end fork_lift_tracker;
	
	system implementation fork_lift_tracker.impl
		subcomponents
			wireless_module: device wireless_module.impl;
			bluetooth_module: device bluetooth_module.impl;
			radio_module: device radio_module.impl;
			
			rfid_reader: device rfid_reader.impl;
			
			send_tracking_info_via_wifi: process send_tracking_info.impl in modes(wifi_enabled);
			send_tracking_info_via_ble: process send_tracking_info.impl in modes(ble_enabled);
			send_tracking_info_via_radio: process send_tracking_info.impl in modes(radio_enabled);
			protocol_management_process: process protocol_management_process.impl;
			
			coordinates: data fork_lift_tracking_system::coordinates;
			tracking_information: data fork_lift_tracking_system::tracking_information;
			is_enabled_at_location_interface: data fork_lift_tracking_system::is_enabled_at_location_interface;
		connections
			data_access_1: data access coordinates -> send_tracking_info_via_wifi.coordinates;
			data_access_2: data access tracking_information -> send_tracking_info_via_wifi.tracking_information;
			data_access_3: data access is_enabled_at_location_interface -> send_tracking_info_via_wifi.is_enabled_at_location_interface;
			data_access_4: data access coordinates -> send_tracking_info_via_ble.coordinates;
			data_access_5: data access tracking_information -> send_tracking_info_via_ble.tracking_information;
			data_access_6: data access is_enabled_at_location_interface -> send_tracking_info_via_ble.is_enabled_at_location_interface;
			data_access_7: data access coordinates -> send_tracking_info_via_radio.coordinates;
			data_access_8: data access tracking_information -> send_tracking_info_via_radio.tracking_information;
			data_access_9: data access is_enabled_at_location_interface -> send_tracking_info_via_radio.is_enabled_at_location_interface;
			
			conn_1: port wireless_module.data_send_event -> wifi_tracking_information;
			conn_2: port bluetooth_module.data_send_event -> bluetooth_tracking_information;
			conn_3: port radio_module.data_send_event -> radio_tracking_information;
			conn_4: port rfid_reader.x -> send_tracking_info_via_wifi.x;
			conn_5: port rfid_reader.y -> send_tracking_info_via_wifi.y;
			conn_6: port rfid_reader.tag_id -> send_tracking_info_via_wifi.tag_id;
			conn_7: port rfid_reader.error_margin -> send_tracking_info_via_wifi.error_margin;
			conn_8: port rfid_reader.x -> send_tracking_info_via_ble.x;
			conn_9: port rfid_reader.y -> send_tracking_info_via_ble.y;
			conn_10: port rfid_reader.tag_id -> send_tracking_info_via_ble.tag_id;
			conn_11: port rfid_reader.error_margin -> send_tracking_info_via_ble.error_margin;
			conn_12: port rfid_reader.x -> send_tracking_info_via_radio.x;
			conn_13: port rfid_reader.y -> send_tracking_info_via_radio.y;
			conn_14: port rfid_reader.tag_id -> send_tracking_info_via_radio.tag_id;
			conn_15: port rfid_reader.error_margin -> send_tracking_info_via_radio.error_margin;
			conn_16: port protocol_management_process.wireless_power_on -> wireless_module.power_on;
			conn_17: port protocol_management_process.wireless_power_off -> wireless_module.power_off;
			conn_18: port wireless_module.signal_strength -> protocol_management_process.wireless_signal_strength;
			conn_19: port send_tracking_info_via_wifi.send_method -> wireless_module.send;
			conn_20: port protocol_management_process.bluetooth_power_on -> bluetooth_module.power_on;
			conn_21: port protocol_management_process.bluetooth_power_off -> bluetooth_module.power_off;
			conn_22: port bluetooth_module.signal_strength -> protocol_management_process.bluetooth_signal_strength;
			conn_23: port send_tracking_info_via_ble.send_method -> bluetooth_module.send;
			conn_24: port protocol_management_process.radio_power_on -> radio_module.power_on;
			conn_25: port protocol_management_process.radio_power_off -> radio_module.power_off;
			conn_26: port radio_module.signal_strength -> protocol_management_process.radio_signal_strength;
			conn_27: port send_tracking_info_via_radio.send_method -> radio_module.send;
		modes
			wifi_enabled -[protocol_management_process.use_ble]-> ble_enabled;
			wifi_enabled -[protocol_management_process.use_radio]-> radio_enabled;
			ble_enabled -[protocol_management_process.use_wifi]-> wifi_enabled;
			ble_enabled -[protocol_management_process.use_radio]-> radio_enabled;
			radio_enabled -[protocol_management_process.use_wifi]-> wifi_enabled;
			radio_enabled -[protocol_management_process.use_ble]-> ble_enabled;
		annex simdase {**
			time_min => 0.0;
			time_max => 9.0;
			variants => {
				"wifi_enabled":{
					"wireless_module","rfid_reader","send_tracking_info_via_wifi","protocol_management_process"
				};
				"ble_enabled":{
					"bluetooth_module","rfid_reader","send_tracking_info_via_ble","protocol_management_process"
				};
				"radio_enabled":{
					"radio_module","rfid_reader","send_tracking_info_via_radio","protocol_management_process"
				};
			};
			active_variants => { "fork_lift_tracker.impl": ["wifi_enabled", "ble_enabled", "radio_enabled"]; };
			cost => {
				organizational_planning => {
					2.0 "Project Manager" for if(t == 0.0) { 40.0 } else { 0.0 }
				};
				requirements_engineering => {
					2.0 "Business Analysts" for if(t == 1.0 || t == 2.0) { 40.0 } else { if(t == 3.0) { 20.0 } else { 0.0 } }
				};
				architecture_definition => {
					2.0 "Software Architects" for if(t == 1.0 || t == 2.0) { 30.0 } else { if(t == 3.0) { 20.0 } else { 0.0 } }
				};
				architecture_evaluation => {
					2.0 "Software Architects" for if(t == 1.0 || t == 2.0) { 10.0 } else { if(t == 3.0) { 5.0 } else { 0.0 } }
				};
				component_development => {
					3.0 "Software Developers" for if(t >= 3.0 && t <= 7.0) { 40.0 } else { 0.0 }
				};
				testing => {
					2.0 "Testers" for if(t >= 7.0 && t <= 9.0) { 40.0 } else { 0.0 }
				};
				software_system_integration => {
					1.0 "System Administrator" for if(t == 9.0) { 40.0 } else { 0.0 }
				};
			};
		**};
	end fork_lift_tracker.impl;
	
	
end fork_lift_tracker;
