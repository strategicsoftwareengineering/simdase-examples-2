package sensors
public
	with Base_Types;
	
	abstract sensor
		features 
			powered_on: in event data port Base_Types::Boolean;
			power_draw: out data port Base_Types::Integer;
			sensor_data: out event data port Base_Types::Integer;
			error_rate: out data port Base_Types::Integer;
		annex agree {**
			guarantee "power draw is between 0 and 100 amps": 0 <= power_draw and power_draw <= 100;
			guarantee "sensor data is between 0 and 100": 0 <= sensor_data and sensor_data <= 100;
			guarantee "error rate is between 0 and 100 percent": 0 <= error_rate and error_rate <= 100;
			guarantee "power draw is 0 if powered off": (powered_on = false and power_draw = 0) or powered_on = true;
		**};
		annex xagree {**
			guarantee "power draw is between 0 and 100 amps": 0 <= power_draw and power_draw <= 100;
			guarantee "error rate is between 0 and 100 percent": 0 <= error_rate and error_rate <= 100;
			guarantee "power draw is 0 if powered off": (powered_on = false and power_draw = 0) or powered_on = true;
		**};
	end sensor;
	
	abstract implementation sensor.impl
		annex agree {**
			node Counter(init:int, incr: int, reset: bool)
				returns(count: int);
			let
				count = if reset then init else prev(count, init)+incr;
			tel;
			assign power_draw = Counter(0, 1, not powered_on or prev(power_draw = 100, false));
			assign sensor_data = Counter(0, 1, prev(sensor_data = 100, false));
			assign error_rate = Counter(0, 1, prev(error_rate = 100, false));
		**};
		annex xagree {**
			node Counter(init:int, incr: int, reset: bool)
				returns(count: int);
			let
				count = if reset then init else prev(count, init)+incr;
			tel;
			eq abstract sens_data: int;
			assign power_draw = Counter(0, 1, not powered_on or prev(power_draw = 100, false));
			assign sensor_data = sens_data;
			assign error_rate = Counter(0, 1, prev(error_rate = 100, false));
		**};
	end sensor.impl;
	
	--outside sensors
	
	device water_temperature_sensor extends sensor
		annex agree {**
			guarantee "power draw is between 0 and 100 amps": 0 <= power_draw and power_draw <= 100;
			guarantee "sensor data is between 20 and 120 degrees": 20 <= sensor_data and sensor_data <= 120;
			guarantee "error rate is between 0 and 100 percent": 0 <= error_rate and error_rate <= 100;
			guarantee "power draw is 0 if powered off": (powered_on = false and power_draw = 0) or powered_on = true;
		**};
		annex xagree {**
			guarantee "sensor data is between 20 and 120 degrees": 20 <= sensor_data and sensor_data <= 120;
		**};
	end water_temperature_sensor;
	
	device implementation water_temperature_sensor.impl extends sensor.impl
		annex agree {**
			node Counter(init:int, incr: int, reset: bool)
				returns(count: int);
			let
				count = if reset then init else prev(count, init)+incr;
			tel;
			assign power_draw = Counter(0, 1, not powered_on or prev(power_draw = 100, false));
			assign sensor_data = Counter(20, 1, prev(sensor_data = 120, false));
			assign error_rate = Counter(0, 1, prev(error_rate = 100, false));
		**};
		annex xagree {**
			eq sens_data: int = Counter(20, 1, prev(sensor_data = 120, false));
		**};
	end water_temperature_sensor.impl;
	
	device water_salinity_sensor extends sensor
		annex agree {**
			guarantee "power draw is between 0 and 100 amps": 0 <= power_draw and power_draw <= 100;
			guarantee "sensor data is between 0 and 50 percent": 0 <= sensor_data and sensor_data <= 50;
			guarantee "error rate is between 0 and 100 percent": 0 <= error_rate and error_rate <= 100;
			guarantee "power draw is 0 if powered off": (powered_on = false and power_draw = 0) or powered_on = true;
		**};
		annex xagree {**
			guarantee "sensor data is between 0 and 50 percent": 0 <= sensor_data and sensor_data <= 50;
		**};
	end water_salinity_sensor;
	
	device implementation water_salinity_sensor.impl extends sensor.impl
		annex agree {**
			node Counter(init:int, incr: int, reset: bool)
				returns(count: int);
			let
				count = if reset then init else prev(count, init)+incr;
			tel;
			assign power_draw = Counter(0, 1, not powered_on or prev(power_draw = 100, false));
			assign sensor_data = Counter(0, 1, prev(sensor_data = 50, false));
			assign error_rate = Counter(0, 1, prev(error_rate = 100, false));
		**};
		annex xagree {**
			eq sens_data: int = Counter(0, 1, prev(sensor_data = 50, false));
		**};
	end water_salinity_sensor.impl;
	
	device radar_sensor extends sensor
		annex agree {**
			guarantee "power draw is between 0 and 100 amps": 0 <= power_draw and power_draw <= 100;
			guarantee "sensor data is between 0 and 100 percent": 0 <= sensor_data and sensor_data <= 100;
			guarantee "error rate is between 0 and 100 percent": 0 <= error_rate and error_rate <= 100;
			guarantee "power draw is 0 if powered off": (powered_on = false and power_draw = 0) or powered_on = true;
		**};
		annex xagree {**
			guarantee "sensor data is between 0 and 100 percent": 0 <= sensor_data and sensor_data <= 100;
		**};
	end radar_sensor;
	
	device implementation radar_sensor.impl extends sensor.impl
		annex agree {**
			node Counter(init:int, incr: int, reset: bool)
				returns(count: int);
			let
				count = if reset then init else prev(count, init)+incr;
			tel;
			assign power_draw = Counter(0, 1, not powered_on or prev(power_draw = 100, false));
			assign sensor_data = Counter(0, 1, prev(sensor_data = 100, false));
			assign error_rate = Counter(0, 1, prev(error_rate = 100, false));
		**};
		annex xagree {**
			eq sens_data: int = Counter(0, 1, prev(sensor_data = 100, false));
		**};
	end radar_sensor.impl;
	
	--outside / inside sensors
	
	device pressure_sensor extends sensor
		annex agree {**
			guarantee "power draw is between 0 and 100 amps": 0 <= power_draw and power_draw <= 100;
			guarantee "sensor data is between 0 and 5000 lbs per square inch": 0 <= sensor_data and sensor_data <= 5000;
			guarantee "error rate is between 0 and 100 percent": 0 <= error_rate and error_rate <= 100;
			guarantee "power draw is 0 if powered off": (powered_on = false and power_draw = 0) or powered_on = true;
		**};
		annex xagree {**
			guarantee "sensor data is between 0 and 5000 lbs per square inch": 0 <= sensor_data and sensor_data <= 5000;
		**};
	end pressure_sensor;
	
	device implementation pressure_sensor.impl extends sensor.impl
		annex agree {**
			node Counter(init:int, incr: int, reset: bool)
				returns(count: int);
			let
				count = if reset then init else prev(count, init)+incr;
			tel;
			assign power_draw = Counter(0, 1, not powered_on or prev(power_draw = 100, false));
			assign sensor_data = Counter(0, 1, prev(sensor_data = 5000, false));
			assign error_rate = Counter(0, 1, prev(error_rate = 100, false));
		**};
		annex xagree {**
			eq sens_data: int = Counter(0, 1, prev(sensor_data = 5000, false));
		**};
	end pressure_sensor.impl;
	
	--inside sensors
	
	device oxygen_sensor extends sensor
		annex agree {**
			guarantee "power draw is between 0 and 100 amps": 0 <= power_draw and power_draw <= 100;
			guarantee "sensor data is between 0 and 50 percent": 0 <= sensor_data and sensor_data <= 50;
			guarantee "error rate is between 0 and 100 percent": 0 <= error_rate and error_rate <= 100;
			guarantee "power draw is 0 if powered off": (powered_on = false and power_draw = 0) or powered_on = true;
		**};
		annex xagree {**
			guarantee "sensor data is between 0 and 50 percent": 0 <= sensor_data and sensor_data <= 50;
		**};
	end oxygen_sensor;
	
	device implementation oxygen_sensor.impl extends sensor.impl
		annex agree {**
			node Counter(init:int, incr: int, reset: bool)
				returns(count: int);
			let
				count = if reset then init else prev(count, init)+incr;
			tel;
			assign power_draw = Counter(0, 1, not powered_on or prev(power_draw = 100, false));
			assign sensor_data = Counter(0, 1, prev(sensor_data = 50, false));
			assign error_rate = Counter(0, 1, prev(error_rate = 100, false));
		**};
		annex xagree {**
			eq sens_data: int = Counter(0, 1, prev(sensor_data = 50, false));
		**};
	end oxygen_sensor.impl;
	
	device carbon_dioxide_sensor extends sensor
		annex agree {**
			guarantee "power draw is between 0 and 100 amps": 0 <= power_draw and power_draw <= 100;
			guarantee "sensor data is between 0 and 50 percent": 0 <= sensor_data and sensor_data <= 50;
			guarantee "error rate is between 0 and 100 percent": 0 <= error_rate and error_rate <= 100;
			guarantee "power draw is 0 if powered off": (powered_on = false and power_draw = 0) or powered_on = true;
		**};
		annex xagree {**
			guarantee "sensor data is between 0 and 50 percent": 0 <= sensor_data and sensor_data <= 50;
		**};
	end carbon_dioxide_sensor;
	
	device implementation carbon_dioxide_sensor.impl extends sensor.impl
		annex agree {**
			node Counter(init:int, incr: int, reset: bool)
				returns(count: int);
			let
				count = if reset then init else prev(count, init)+incr;
			tel;
			assign power_draw = Counter(0, 1, not powered_on or prev(power_draw = 100, false));
			assign sensor_data = Counter(0, 1, prev(sensor_data = 50, false));
			assign error_rate = Counter(0, 1, prev(error_rate = 100, false));
		**};
		annex xagree {**
			eq sens_data: int = Counter(0, 1, prev(sensor_data = 50, false));
		**};
	end carbon_dioxide_sensor.impl;
	
	device carbon_monoxide_sensor extends sensor
		annex agree {**
			guarantee "power draw is between 0 and 100 amps": 0 <= power_draw and power_draw <= 100;
			guarantee "sensor data is between 0 and 50 percent": 0 <= sensor_data and sensor_data <= 50;
			guarantee "error rate is between 0 and 100 percent": 0 <= error_rate and error_rate <= 100;
			guarantee "power draw is 0 if powered off": (powered_on = false and power_draw = 0) or powered_on = true;
		**};
		annex xagree {**
			guarantee "sensor data is between 0 and 50 percent": 0 <= sensor_data and sensor_data <= 50;
		**};
	end carbon_monoxide_sensor;
	
	device implementation carbon_monoxide_sensor.impl extends sensor.impl
		annex agree {**
			node Counter(init:int, incr: int, reset: bool)
				returns(count: int);
			let
				count = if reset then init else prev(count, init)+incr;
			tel;
			assign power_draw = Counter(0, 1, not powered_on or prev(power_draw = 100, false));
			assign sensor_data = Counter(0, 1, prev(sensor_data = 50, false));
			assign error_rate = Counter(0, 1, prev(error_rate = 100, false));
		**};
		annex xagree {**
			eq sens_data: int = Counter(0, 1, prev(sensor_data = 50, false));
		**};
	end carbon_monoxide_sensor.impl;
	
end sensors;
