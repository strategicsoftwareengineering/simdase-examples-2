package fork_lift_tracking_system
public
	with fork_lift_tracker, fork_lift_tracking_server, Base_Types;
	
	subprogram build_coordinates
		features
			x: in parameter Base_Types::Float_64;
			y: in parameter Base_Types::Float_64;
			tag_id: in parameter Base_Types::String;
			error_margin: in parameter Base_Types::Float_64;
			coordinates: out parameter fork_lift_tracking_system::coordinates;
	end build_coordinates;
	
	subprogram compute_speed
		features
			speed: out parameter Base_Types::Float_64;
	end compute_speed;
	
	subprogram build_tracking_information
		features
			coordinates: in parameter coordinates;
			speed: in parameter Base_Types::Float_64;
			tracking_information: out parameter tracking_information;
	end build_tracking_information;
	
	data coordinates
		features
			x: feature Base_Types::Float_64;
			y: feature Base_Types::Float_64;
			tag_id: feature Base_Types::String;
			error_margin: feature Base_Types::Float_64;
			build_coordinates: provides subprogram access build_coordinates;
			compute_speed: provides subprogram access compute_speed;
			build_tracking_information: provides subprogram access build_tracking_information;
	end coordinates;
	
	subprogram get_location
		features
			tracking_information: in parameter tracking_information;
			location: out parameter coordinates;
	end get_location;
	
	subprogram get_speed
		features
			tracking_information: in parameter tracking_information;
			speed: out parameter Base_Types::Float_64;
	end get_speed;
	
	data tracking_information
		features
			location: feature coordinates;
			speed: feature Base_Types::Float_64;
			get_location: provides subprogram access get_location;
			get_speed: provides subprogram access get_speed;
	end tracking_information;
	
	subprogram is_enabled_at_location
		features
			location: in parameter coordinates;
			enabled: out parameter Base_Types::Boolean;
	end is_enabled_at_location;
	
	subprogram is_wireless_enabled_at_location extends is_enabled_at_location
	end is_wireless_enabled_at_location;
	
	subprogram is_bluetooth_enabled_at_location extends is_enabled_at_location
	end is_bluetooth_enabled_at_location;
	
	subprogram is_radio_enabled_at_location extends is_enabled_at_location
	end is_radio_enabled_at_location;
	
	data is_enabled_at_location_interface
		features
			is_wireless_enabled_at_location: provides subprogram access is_wireless_enabled_at_location;
			is_bluetooth_enabled_at_location: provides subprogram access is_bluetooth_enabled_at_location;
			is_radio_enabled_at_location: provides subprogram access is_radio_enabled_at_location;
	end is_enabled_at_location_interface;
	
	-- top level system
	system fork_lift_tracking_system
	end fork_lift_tracking_system;
	
	system implementation fork_lift_tracking_system.impl
		subcomponents
			server: system fork_lift_tracking_server::fork_lift_tracking_server.impl;
			trackers: system fork_lift_tracker::fork_lift_tracker.impl[50];
		connections
			trackers_to_server_wifi: port trackers.wifi_tracking_information -> server.wifi_tracking_information { Connection_Pattern => ((All_To_One)); };
			trackers_to_server_bluetooth: port trackers.bluetooth_tracking_information -> server.bluetooth_tracking_information { Connection_Pattern => ((All_To_One)); };
			trackers_to_server_radio: port trackers.radio_tracking_information -> server.radio_tracking_information { Connection_Pattern => ((All_To_One)); };
	end fork_lift_tracking_system.impl;
	
end fork_lift_tracking_system;
