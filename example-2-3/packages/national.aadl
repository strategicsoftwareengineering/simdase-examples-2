package national
public
	with Base_Types, community, common, family;
	
	abstract national_gatherer extends common::consumer_gatherer
	end national_gatherer;
	
	abstract implementation national_gatherer.impl extends common::consumer_gatherer.impl
		subcomponents
			common_1: refined to abstract community::community_data.impl;
			--common_2: refined to abstract community::community_data.impl;
			--common_3: refined to abstract community::community_data.impl;
			--common_4: refined to abstract community::community_data.impl;
	end national_gatherer.impl;
	
	abstract national_redistributor extends common::abstract_redistributor
	end national_redistributor;
	
	abstract implementation national_redistributor.impl extends common::abstract_redistributor.impl
		--subcomponents
			--common_1: refined to abstract community::community.impl;
			--common_2: refined to abstract community::community.impl;
			--common_3: refined to abstract community::community.impl;
			--common_4: refined to abstract community::community.impl;
	end national_redistributor.impl;
	
	abstract national_store_for_later_redistributor extends common::abstract_redistributor
	end national_store_for_later_redistributor;
	
	abstract implementation national_store_for_later_redistributor.impl extends common::abstract_redistributor.impl
		--subcomponents
			--common_1: refined to abstract community::community.impl;
			--common_2: refined to abstract community::community.impl;
			--common_3: refined to abstract community::community.impl;
			--common_4: refined to abstract community::community.impl;
			--veggie_divider: refined to abstract common::divider_and_storer.impl;
			--bread_divider: refined to abstract common::divider_and_storer.impl;
			--dairy_divider: refined to abstract common::divider_and_storer.impl;
			--meat_divider: refined to abstract common::divider_and_storer.impl;
	end national_store_for_later_redistributor.impl;
	
	abstract national_consumers
		features
			vegetables_received: in data port Base_Types::Integer;
			bread_received: in data port Base_Types::Integer;
			dairy_received: in data port Base_Types::Integer;
			meat_received: in data port Base_Types::Integer;
	end national_consumers;
	
	abstract implementation national_consumers.impl
		subcomponents
			national_gatherer: abstract national_gatherer.impl;
			national_redistributor: abstract national_redistributor.impl;
			national_redistributor_with_store: abstract national_store_for_later_redistributor.impl;
		connections
			conn_1: port vegetables_received -> national_redistributor.total_vegetables_received;
			conn_2: port bread_received -> national_redistributor.total_bread_received;
			conn_3: port dairy_received -> national_redistributor.total_dairy_received;
			conn_4: port meat_received -> national_redistributor.total_meat_received;
			
			conn_5: port vegetables_received -> national_redistributor_with_store.total_vegetables_received;
			conn_6: port bread_received -> national_redistributor_with_store.total_bread_received;
			conn_7: port dairy_received -> national_redistributor_with_store.total_dairy_received;
			conn_8: port meat_received -> national_redistributor_with_store.total_meat_received;
			
			conn_9: port national_gatherer.unit_1_vegetables_needed -> national_redistributor.unit_1_vegetables_needed;
			--conn_10: port national_gatherer.unit_2_vegetables_needed -> national_redistributor.unit_2_vegetables_needed;
			--conn_11: port national_gatherer.unit_3_vegetables_needed -> national_redistributor.unit_3_vegetables_needed;
			--conn_12: port national_gatherer.unit_4_vegetables_needed -> national_redistributor.unit_4_vegetables_needed;
			conn_13: port national_gatherer.unit_1_bread_needed -> national_redistributor.unit_1_bread_needed;
			--conn_14: port national_gatherer.unit_2_bread_needed -> national_redistributor.unit_2_bread_needed;
			--conn_15: port national_gatherer.unit_3_bread_needed -> national_redistributor.unit_3_bread_needed;
			--conn_16: port national_gatherer.unit_4_bread_needed -> national_redistributor.unit_4_bread_needed;
			conn_17: port national_gatherer.unit_1_dairy_needed -> national_redistributor.unit_1_dairy_needed;
			--conn_18: port national_gatherer.unit_2_dairy_needed -> national_redistributor.unit_2_dairy_needed;
			--conn_19: port national_gatherer.unit_3_dairy_needed -> national_redistributor.unit_3_dairy_needed;
			--conn_20: port national_gatherer.unit_4_dairy_needed -> national_redistributor.unit_4_dairy_needed;
			conn_21: port national_gatherer.unit_1_meat_needed -> national_redistributor.unit_1_meat_needed;
			--conn_22: port national_gatherer.unit_2_meat_needed -> national_redistributor.unit_2_meat_needed;
			--conn_23: port national_gatherer.unit_3_meat_needed -> national_redistributor.unit_3_meat_needed;
			--conn_24: port national_gatherer.unit_4_meat_needed -> national_redistributor.unit_4_meat_needed;
			
			conn_25: port national_gatherer.unit_1_vegetables_needed -> national_redistributor_with_store.unit_1_vegetables_needed;
			--conn_26: port national_gatherer.unit_2_vegetables_needed -> national_redistributor_with_store.unit_2_vegetables_needed;
			--conn_27: port national_gatherer.unit_3_vegetables_needed -> national_redistributor_with_store.unit_3_vegetables_needed;
			--conn_28: port national_gatherer.unit_4_vegetables_needed -> national_redistributor_with_store.unit_4_vegetables_needed;
			conn_29: port national_gatherer.unit_1_bread_needed -> national_redistributor_with_store.unit_1_bread_needed;
			--conn_30: port national_gatherer.unit_2_bread_needed -> national_redistributor_with_store.unit_2_bread_needed;
			--conn_31: port national_gatherer.unit_3_bread_needed -> national_redistributor_with_store.unit_3_bread_needed;
			--conn_32: port national_gatherer.unit_4_bread_needed -> national_redistributor_with_store.unit_4_bread_needed;
			conn_33: port national_gatherer.unit_1_dairy_needed -> national_redistributor_with_store.unit_1_dairy_needed;
			--conn_34: port national_gatherer.unit_2_dairy_needed -> national_redistributor_with_store.unit_2_dairy_needed;
			--conn_35: port national_gatherer.unit_3_dairy_needed -> national_redistributor_with_store.unit_3_dairy_needed;
			--conn_36: port national_gatherer.unit_4_dairy_needed -> national_redistributor_with_store.unit_4_dairy_needed;
			conn_37: port national_gatherer.unit_1_meat_needed -> national_redistributor_with_store.unit_1_meat_needed;
			--conn_38: port national_gatherer.unit_2_meat_needed -> national_redistributor_with_store.unit_2_meat_needed;
			--conn_39: port national_gatherer.unit_3_meat_needed -> national_redistributor_with_store.unit_3_meat_needed;
			--conn_40: port national_gatherer.unit_4_meat_needed -> national_redistributor_with_store.unit_4_meat_needed;
	end national_consumers.impl;
	
end national;
