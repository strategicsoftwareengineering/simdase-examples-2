package common
public
	with Base_Types, individual, family;
	
	abstract common_object
		features
			total_vegetables_needed: out data port Base_Types::Integer;
			total_bread_needed: out data port Base_Types::Integer;
			total_dairy_needed: out data port Base_Types::Integer;
			total_meat_needed: out data port Base_Types::Integer;
			
			vegetables_received: in data port Base_Types::Integer;
			bread_received: in data port Base_Types::Integer;
			dairy_received: in data port Base_Types::Integer;
			meat_received: in data port Base_Types::Integer;
	end common_object;
	
	abstract meal_planner_abstract
		features
			total_vegetables_needed: out data port Base_Types::Integer;
			total_bread_needed: out data port Base_Types::Integer;
			total_dairy_needed: out data port Base_Types::Integer;
			total_meat_needed: out data port Base_Types::Integer;
	end meal_planner_abstract;
	
	abstract implementation meal_planner_abstract.impl
		--subcomponents
			--member_1: abstract individual::individual.impl;
			--member_2: abstract individual::individual.impl;
			--member_3: abstract individual::individual.impl;
			--member_4: abstract individual::individual.impl;
	end meal_planner_abstract.impl;
	
	abstract meal_planner extends meal_planner_abstract
	end meal_planner;
	
	abstract implementation meal_planner.impl extends meal_planner_abstract.impl
		subcomponents
			member_1: system individual::individual.impl;
			--member_2: refined to system individual::individual.impl;
			--member_3: refined to system individual::individual.impl;
			--member_4: refined to system individual::individual.impl;
		annex simdase {**
			time_min => 1.0;
			time_max => 52.0;
			cost => {
				requirements_engineering => {
					2.0 "Business Analysts" for if(1.0 <= t && t <= 8.0) { 40.0 } else { 0.0 }	
				};
				architecture_definition => {
					2.0 "Software Architects" for if(8.0 <= t && t <= 20.0) { 40.0 } else { 0.0 }	
				};
				component_development => {
					3.0 "Software Developers" for if(20.0 <= t && t <= 45.0) { 40.0 } else { 0.0 }	
				};
				testing => {
					2.0 "Software Testers" for if(45.0 <= t && t <= 52.0) { 40.0 } else { 0.0 },
					2.0 "Hardware Testers" for if(45.0 <= t && t <= 52.0) { 40.0 } else { 0.0 }	
				};
			};
		**};
	end meal_planner.impl;
	
	abstract allergy_meal_planner extends meal_planner_abstract
	end allergy_meal_planner;
	
	abstract implementation allergy_meal_planner.impl extends meal_planner_abstract.impl
		--subcomponents
			--member_1: refined to system individual::individual.impl;
			--member_2: refined to system individual::individual.impl;
			--member_3: refined to system individual::individual.impl;
			--member_4: refined to system individual::individual.impl;
		annex simdase {**
			time_min => 1.0;
			time_max => 52.0;
			cost => {
				requirements_engineering => {
					2.0 "Business Analysts" for if(1.0 <= t && t <= 8.0) { 40.0 } else { 0.0 }	
				};
				architecture_definition => {
					2.0 "Software Architects" for if(8.0 <= t && t <= 20.0) { 40.0 } else { 0.0 }	
				};
				component_development => {
					3.0 "Software Developers" for if(20.0 <= t && t <= 45.0) { 40.0 } else { 0.0 }	
				};
				testing => {
					2.0 "Software Testers" for if(45.0 <= t && t <= 52.0) { 40.0 } else { 0.0 },
					2.0 "Hardware Testers" for if(45.0 <= t && t <= 52.0) { 40.0 } else { 0.0 }	
				};
			};
		**};
	end allergy_meal_planner.impl;
	
	abstract preference_meal_planner extends meal_planner_abstract
	end preference_meal_planner;
	
	abstract implementation preference_meal_planner.impl extends meal_planner_abstract.impl
		--subcomponents
			--member_1: refined to system individual::individual.impl;
			--member_2: refined to system individual::individual.impl;
			--member_3: refined to system individual::individual.impl;
			--member_4: refined to system individual::individual.impl;
		annex simdase {**
			time_min => 1.0;
			time_max => 52.0;
			cost => {
				requirements_engineering => {
					2.0 "Business Analysts" for if(1.0 <= t && t <= 8.0) { 40.0 } else { 0.0 }	
				};
				architecture_definition => {
					2.0 "Software Architects" for if(8.0 <= t && t <= 20.0) { 40.0 } else { 0.0 }	
				};
				component_development => {
					3.0 "Software Developers" for if(20.0 <= t && t <= 45.0) { 40.0 } else { 0.0 }	
				};
				testing => {
					2.0 "Software Testers" for if(45.0 <= t && t <= 52.0) { 40.0 } else { 0.0 },
					2.0 "Hardware Testers" for if(45.0 <= t && t <= 52.0) { 40.0 } else { 0.0 }	
				};
			};
		**};
	end preference_meal_planner.impl;
	
	abstract preference_allergy_meal_planner extends meal_planner_abstract
	end preference_allergy_meal_planner;
	
	abstract implementation preference_allergy_meal_planner.impl extends meal_planner_abstract.impl
		--subcomponents
			--member_1: refined to system individual::individual.impl;
			--member_2: refined to system individual::individual.impl;
			--member_3: refined to system individual::individual.impl;
			--member_4: refined to system individual::individual.impl;
		annex simdase {**
			time_min => 1.0;
			time_max => 52.0;
			cost => {
				requirements_engineering => {
					2.0 "Business Analysts" for if(1.0 <= t && t <= 8.0) { 40.0 } else { 0.0 }	
				};
				architecture_definition => {
					2.0 "Software Architects" for if(8.0 <= t && t <= 20.0) { 40.0 } else { 0.0 }	
				};
				component_development => {
					3.0 "Software Developers" for if(20.0 <= t && t <= 45.0) { 40.0 } else { 0.0 }	
				};
				testing => {
					2.0 "Software Testers" for if(45.0 <= t && t <= 52.0) { 40.0 } else { 0.0 },
					2.0 "Hardware Testers" for if(45.0 <= t && t <= 52.0) { 40.0 } else { 0.0 }	
				};
			};
		**};
	end preference_allergy_meal_planner.impl;
	
	abstract consumer_gatherer
		features
			total_vegetables_needed: out data port Base_Types::Integer;
			unit_1_vegetables_needed: out data port Base_Types::Integer;
			--unit_2_vegetables_needed: out data port Base_Types::Integer;
			--unit_3_vegetables_needed: out data port Base_Types::Integer;
			--unit_4_vegetables_needed: out data port Base_Types::Integer;
			total_bread_needed: out data port Base_Types::Integer;
			unit_1_bread_needed: out data port Base_Types::Integer;
			--unit_2_bread_needed: out data port Base_Types::Integer;
			--unit_3_bread_needed: out data port Base_Types::Integer;
			--unit_4_bread_needed: out data port Base_Types::Integer;
			total_dairy_needed: out data port Base_Types::Integer;
			unit_1_dairy_needed: out data port Base_Types::Integer;
			--unit_2_dairy_needed: out data port Base_Types::Integer;
			--unit_3_dairy_needed: out data port Base_Types::Integer;
			--unit_4_dairy_needed: out data port Base_Types::Integer;
			total_meat_needed: out data port Base_Types::Integer;
			unit_1_meat_needed: out data port Base_Types::Integer;
			--unit_2_meat_needed: out data port Base_Types::Integer;
			--unit_3_meat_needed: out data port Base_Types::Integer;
			--unit_4_meat_needed: out data port Base_Types::Integer;
	end consumer_gatherer;
	
	abstract implementation consumer_gatherer.impl
		subcomponents
			common_1: abstract common_object;
			--common_2: abstract common_object;
			--common_3: abstract common_object;
			--common_4: abstract common_object;
		connections
			conn_1: port common_1.total_vegetables_needed -> unit_1_vegetables_needed;
			--conn_2: port common_2.total_vegetables_needed -> unit_2_vegetables_needed;
			--conn_3: port common_3.total_vegetables_needed -> unit_3_vegetables_needed;
			--conn_4: port common_4.total_vegetables_needed -> unit_4_vegetables_needed;
			conn_5: port common_1.total_bread_needed -> unit_1_bread_needed;
			--conn_6: port common_2.total_bread_needed -> unit_2_bread_needed;
			--conn_7: port common_3.total_bread_needed -> unit_3_bread_needed;
			--conn_8: port common_4.total_bread_needed -> unit_4_bread_needed;
			conn_9: port common_1.total_dairy_needed -> unit_1_dairy_needed;
			--conn_10: port common_2.total_dairy_needed -> unit_2_dairy_needed;
			--conn_11: port common_3.total_dairy_needed -> unit_3_dairy_needed;
			--conn_12: port common_4.total_dairy_needed -> unit_4_dairy_needed;
			conn_13: port common_1.total_meat_needed -> unit_1_meat_needed;
			--conn_14: port common_2.total_meat_needed -> unit_2_meat_needed;
			--conn_15: port common_3.total_meat_needed -> unit_3_meat_needed;
			--conn_16: port common_4.total_meat_needed -> unit_4_meat_needed;
		annex simdase {**
			time_min => 1.0;
			time_max => 78.0;
			scaling_factor => 1.0/(i+1.0);
			cost => {
				requirements_engineering => {
					2.0 "Business Analysts" for if(1.0 <= t && t <= 12.0) { 40.0 } else { 0.0 }	
				};
				architecture_definition => {
					2.0 "Software Architects" for if(12.0 <= t && t <= 30.0) { 40.0 } else { 0.0 }	
				};
				component_development => {
					3.0 "Software Developers" for if(30.0 <= t && t <= 65.0) { 40.0 } else { 0.0 }	
				};
				testing => {
					2.0 "Software Testers" for if(65.0 <= t && t <= 78.0) { 40.0 } else { 0.0 },
					2.0 "Hardware Testers" for if(65.0 <= t && t <= 78.0) { 40.0 } else { 0.0 }	
				};
			};
		**};
	end consumer_gatherer.impl;
	
	abstract producer_gatherer
		features
			total_vegetables_produced: out data port Base_Types::Integer;
			total_bread_produced: out data port Base_Types::Integer;
			total_dairy_produced: out data port Base_Types::Integer;
			total_meat_produced: out data port Base_Types::Integer;
	end producer_gatherer;
	
	abstract implementation producer_gatherer.impl
		subcomponents
			family_1: abstract family::farmer.impl;
			--family_2: abstract family::farmer.impl;
			--family_3: abstract family::farmer.impl;
			--family_4: abstract family::farmer.impl;
		connections
			conn_1: port family_1.vegetables_produced -> total_vegetables_produced;
			--conn_2: port family_2.vegetables_produced -> total_vegetables_produced;
			--conn_3: port family_3.vegetables_produced -> total_vegetables_produced;
			--conn_4: port family_4.vegetables_produced -> total_vegetables_produced;
			conn_5: port family_1.bread_produced -> total_bread_produced;
			--conn_6: port family_2.bread_produced -> total_bread_produced;
			--conn_7: port family_3.bread_produced -> total_bread_produced;
			--conn_8: port family_4.bread_produced -> total_bread_produced;
			conn_9: port family_1.dairy_produced -> total_dairy_produced;
			--conn_10: port family_2.dairy_produced -> total_dairy_produced;
			--conn_11: port family_3.dairy_produced -> total_dairy_produced;
			--conn_12: port family_4.dairy_produced -> total_dairy_produced;
			conn_13: port family_1.meat_produced -> total_meat_produced;
			--conn_14: port family_2.meat_produced -> total_meat_produced;
			--conn_15: port family_3.meat_produced -> total_meat_produced;
			--conn_16: port family_4.meat_produced -> total_meat_produced;
		annex simdase {**
			time_min => 1.0;
			time_max => 78.0;
			scaling_factor => 1.0/(i+1.0);
			cost => {
				requirements_engineering => {
					2.0 "Business Analysts" for if(1.0 <= t && t <= 12.0) { 40.0 } else { 0.0 }	
				};
				architecture_definition => {
					2.0 "Software Architects" for if(12.0 <= t && t <= 30.0) { 40.0 } else { 0.0 }	
				};
				component_development => {
					3.0 "Software Developers" for if(30.0 <= t && t <= 65.0) { 40.0 } else { 0.0 }	
				};
				testing => {
					2.0 "Software Testers" for if(65.0 <= t && t <= 78.0) { 40.0 } else { 0.0 },
					2.0 "Hardware Testers" for if(65.0 <= t && t <= 78.0) { 40.0 } else { 0.0 }	
				};
			};
		**};
	end producer_gatherer.impl;
	
	abstract abstract_redistributor
		features
			total_vegetables_received: in data port Base_Types::Integer;
			unit_1_vegetables_needed: in data port Base_Types::Integer;
			--unit_2_vegetables_needed: in data port Base_Types::Integer;
			--unit_3_vegetables_needed: in data port Base_Types::Integer;
			--unit_4_vegetables_needed: in data port Base_Types::Integer;
			total_bread_received: in data port Base_Types::Integer;
			unit_1_bread_needed: in data port Base_Types::Integer;
			--unit_2_bread_needed: in data port Base_Types::Integer;
			--unit_3_bread_needed: in data port Base_Types::Integer;
			--unit_4_bread_needed: in data port Base_Types::Integer;
			total_dairy_received: in data port Base_Types::Integer;
			unit_1_dairy_needed: in data port Base_Types::Integer;
			--unit_2_dairy_needed: in data port Base_Types::Integer;
			--unit_3_dairy_needed: in data port Base_Types::Integer;
			--unit_4_dairy_needed: in data port Base_Types::Integer;
			total_meat_received: in data port Base_Types::Integer;
			unit_1_meat_needed: in data port Base_Types::Integer;
			--unit_2_meat_needed: in data port Base_Types::Integer;
			--unit_3_meat_needed: in data port Base_Types::Integer;
			--unit_4_meat_needed: in data port Base_Types::Integer;
	end abstract_redistributor;
	
	abstract implementation abstract_redistributor.impl
		--subcomponents
			--common_1: abstract common_object;
			--common_2: abstract common_object;
			--common_3: abstract common_object;
			--common_4: abstract common_object;
			--veggie_divider: abstract divider.impl;
			--bread_divider: abstract divider.impl;
			--dairy_divider: abstract divider.impl;
			--meat_divider: abstract divider.impl;
		--connections
			--conn_1: port total_vegetables_received -> veggie_divider.total_received;
			--conn_2: port total_bread_received -> bread_divider.total_received;
			--conn_3: port total_dairy_received -> dairy_divider.total_received;
			--conn_4: port total_meat_received -> meat_divider.total_received;
			--conn_5: port unit_1_vegetables_needed -> veggie_divider.unit_1_needed;
			--conn_6: port unit_2_vegetables_needed -> veggie_divider.unit_2_needed;
			--conn_7: port unit_3_vegetables_needed -> veggie_divider.unit_3_needed;
			--conn_8: port unit_4_vegetables_needed -> veggie_divider.unit_4_needed;
			--conn_9: port veggie_divider.unit_1_receives -> common_1.vegetables_received;
			--conn_10: port veggie_divider.unit_2_receives -> common_2.vegetables_received;
			--conn_11: port veggie_divider.unit_3_receives -> common_3.vegetables_received;
			--conn_12: port veggie_divider.unit_4_receives -> common_4.vegetables_received;
			
			--conn_13: port unit_1_bread_needed -> bread_divider.unit_1_needed;
			--conn_14: port unit_2_bread_needed -> bread_divider.unit_2_needed;
			--conn_15: port unit_3_bread_needed -> bread_divider.unit_3_needed;
			--conn_16: port unit_4_bread_needed -> bread_divider.unit_4_needed;
			--conn_17: port bread_divider.unit_1_receives -> common_1.bread_received;
			--conn_18: port bread_divider.unit_2_receives -> common_2.bread_received;
			--conn_19: port bread_divider.unit_3_receives -> common_3.bread_received;
			--conn_20: port bread_divider.unit_4_receives -> common_4.bread_received;
			
			--conn_21: port unit_1_dairy_needed -> dairy_divider.unit_1_needed;
			--conn_22: port unit_2_dairy_needed -> dairy_divider.unit_2_needed;
			--conn_23: port unit_3_dairy_needed -> dairy_divider.unit_3_needed;
			--conn_24: port unit_4_dairy_needed -> dairy_divider.unit_4_needed;
			--conn_25: port dairy_divider.unit_1_receives -> common_1.dairy_received;
			--conn_26: port dairy_divider.unit_2_receives -> common_2.dairy_received;
			--conn_27: port dairy_divider.unit_3_receives -> common_3.dairy_received;
			--conn_28: port dairy_divider.unit_4_receives -> common_4.dairy_received;
			
			--conn_29: port unit_1_meat_needed -> meat_divider.unit_1_needed;
			--conn_30: port unit_2_meat_needed -> meat_divider.unit_2_needed;
			--conn_31: port unit_3_meat_needed -> meat_divider.unit_3_needed;
			--conn_32: port unit_4_meat_needed -> meat_divider.unit_4_needed;
			--conn_33: port meat_divider.unit_1_receives -> common_1.meat_received;
			--conn_34: port meat_divider.unit_2_receives -> common_2.meat_received;
			--conn_35: port meat_divider.unit_3_receives -> common_3.meat_received;
			--conn_36: port meat_divider.unit_4_receives -> common_4.meat_received;
			
			--conn_37: port unit_1_vegetables_needed -> common_1.vegetables_received;
			--conn_38: port unit_2_vegetables_needed -> common_2.vegetables_received;
			--conn_39: port unit_3_vegetables_needed -> common_3.vegetables_received;
			--conn_40: port unit_4_vegetables_needed -> common_4.vegetables_received;
			--conn_41: port unit_1_bread_needed -> common_1.bread_received;
			--conn_42: port unit_2_bread_needed -> common_2.bread_received;
			--conn_43: port unit_3_bread_needed -> common_3.bread_received;
			--conn_44: port unit_4_bread_needed -> common_4.bread_received;
			--conn_45: port unit_1_dairy_needed -> common_1.dairy_received;
			--conn_46: port unit_2_dairy_needed -> common_2.dairy_received;
			--conn_47: port unit_3_dairy_needed -> common_3.dairy_received;
			--conn_48: port unit_4_dairy_needed -> common_4.dairy_received;
			--conn_49: port unit_1_meat_needed -> common_1.meat_received;
			--conn_50: port unit_2_meat_needed -> common_2.meat_received;
			--conn_51: port unit_3_meat_needed -> common_3.meat_received;
			--conn_52: port unit_4_meat_needed -> common_4.meat_received;
		annex simdase {**
			time_min => 1.0;
			time_max => 78.0;
			scaling_factor => 1.0/(i+1.0);
			cost => {
				requirements_engineering => {
					2.0 "Business Analysts" for if(1.0 <= t && t <= 12.0) { 40.0 } else { 0.0 }	
				};
				architecture_definition => {
					2.0 "Software Architects" for if(12.0 <= t && t <= 30.0) { 40.0 } else { 0.0 }	
				};
				component_development => {
					3.0 "Software Developers" for if(30.0 <= t && t <= 65.0) { 40.0 } else { 0.0 }	
				};
				testing => {
					2.0 "Software Testers" for if(65.0 <= t && t <= 78.0) { 40.0 } else { 0.0 },
					2.0 "Hardware Testers" for if(65.0 <= t && t <= 78.0) { 40.0 } else { 0.0 }	
				};
			};
		**};
	end abstract_redistributor.impl;
	
	abstract divider 
		features
			total_received: in data port Base_Types::Integer;
			unit_1_needed: in data port Base_Types::Integer;
			unit_2_needed: in data port Base_Types::Integer;
			unit_3_needed: in data port Base_Types::Integer;
			unit_4_needed: in data port Base_Types::Integer;
			
			unit_1_receives: out data port Base_Types::Integer;
			unit_2_receives: out data port Base_Types::Integer;
			unit_3_receives: out data port Base_Types::Integer;
			unit_4_receives: out data port Base_Types::Integer;
	end divider;
	
	abstract implementation divider.impl
	end divider.impl;
	
	abstract divider_and_storer extends divider
	end divider_and_storer;
	
	abstract implementation divider_and_storer.impl extends divider.impl
	end divider_and_storer.impl;
	
end common;
